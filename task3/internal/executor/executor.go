package executor

import (
	"fmt"
	"task3/pkg/models/enroll"
	"task3/pkg/models/transfer"
	"task3/pkg/models/user"
	"task3/pkg/storage"
)

type Executor struct {
	storage storage.Storage[user.User]
}

func NewExecutor(s storage.Storage[user.User]) *Executor {
	return &Executor{storage: s}
}

func (e *Executor) GetUserById(id int) (user.User, error) {
	return e.storage.GetUserById(id)
}

func (e *Executor) Transfer(trans transfer.Transfer) error {
	if trans.Sender == trans.Destination {
		return fmt.Errorf("you can't transfer money to yourself")
	}

	if trans.Amount <= 0 {
		return fmt.Errorf("bad amount")
	}

	sender, err := e.GetUserById(trans.Sender)
	if err != nil {
		return err
	}

	destination, err := e.GetUserById(trans.Destination)
	if err != nil {
		return err
	}

	if sender.Balance < trans.Amount {
		return fmt.Errorf("not enough money")
	}
	sender.Balance -= trans.Amount
	destination.Balance += trans.Amount
	e.storage.Save(sender)
	e.storage.Save(destination)

	return nil
}

func (e *Executor) Enroll(en enroll.Enroll) error {
	if en.Amount <= 0 {
		return fmt.Errorf("bad amount")
	}

	usr, err := e.GetUserById(en.Id)
	if err != nil {
		return nil
	}

	usr.Balance += en.Amount
	e.storage.Save(usr)

	return nil
}
