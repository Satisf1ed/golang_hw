package storage

import (
	"fmt"
	"task3/pkg/models/user"
	"task3/pkg/storage"
)

var _ storage.Storage[user.User] = (*Storage)(nil)

type Storage struct {
	users map[int]user.User
}

func NewStorage() *Storage {
	usersMap := make(map[int]user.User)
	usersMap[0] = user.User{Id: 0, Balance: 1980}
	usersMap[1] = user.User{Id: 1, Balance: 0}
	usersMap[121] = user.User{Id: 121, Balance: 300.01}

	store := &Storage{users: usersMap}
	return store
}

func (s *Storage) GetUserById(id int) (user.User, error) {
	usr, err := s.users[id]
	if !err {
		return user.User{}, fmt.Errorf("not found")
	}
	return usr, nil
}

func (s *Storage) Save(usr user.User) {
	s.users[usr.Id] = usr
}
