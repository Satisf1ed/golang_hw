package handlers

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"task3/internal/executor"
	"task3/pkg/models/enroll"
	"task3/pkg/models/transfer"
)

type Controller struct {
	exec *executor.Executor
}

func NewController(e *executor.Executor) *Controller {
	return &Controller{exec: e}
}

func (c *Controller) GetBalance(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	usr, err := c.exec.GetUserById(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	result, err := json.Marshal(usr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_, _ = w.Write(result)
}

func (c *Controller) TransferMoney(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	trans := transfer.Transfer{}

	err = json.Unmarshal(body, &trans)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = c.exec.Transfer(trans)
	if err != nil {
		_, _ = w.Write([]byte(err.Error()))
	}
}

func (c *Controller) EnrollMoney(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	en := enroll.Enroll{}

	err = json.Unmarshal(body, &en)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = c.exec.Enroll(en)
	if err != nil {
		_, _ = w.Write([]byte(err.Error()))
	}
}
