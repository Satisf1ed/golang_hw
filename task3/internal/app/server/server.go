package server

import (
	"context"
	"fmt"
	"net/http"
	"task3/internal/executor"
	"task3/internal/handlers"
	"task3/internal/storage"
)

type App struct {
	server *http.Server
}

func initServer() http.Handler {
	serverMux := http.NewServeMux()

	handler := handlers.NewController(executor.NewExecutor(storage.NewStorage()))

	serverMux.HandleFunc("/info", handler.GetBalance)
	serverMux.HandleFunc("/transfer", handler.TransferMoney)
	serverMux.HandleFunc("/enroll", handler.EnrollMoney)

	return serverMux
}

func NewApp(address string) *App {
	newServer := &http.Server{
		Addr:    address,
		Handler: initServer(),
	}

	return &App{
		server: newServer,
	}
}

func (a *App) Run() {
	go func() {
		err := a.server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
}

func (a *App) Stop(ctx context.Context) {
	fmt.Println(a.server.Shutdown(ctx))
}
