package transaction

import "task3/pkg/models/user"

type Transaction[T any] interface {
	GetUserById(id int) (user.User, error)
}
