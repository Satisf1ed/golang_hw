package storage

import "task3/pkg/models/user"

type Storage[T any] interface {
	GetUserById(id int) (user.User, error)
	Save(usr user.User)
}
