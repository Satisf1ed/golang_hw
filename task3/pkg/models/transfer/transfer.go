package transfer

type Transfer struct {
	Sender      int     `json:"sender"`
	Destination int     `json:"destination"`
	Amount      float32 `json:"amount"`
}
