package user

type User struct {
	Id      int     `json:"user_id"`
	Balance float32 `json:"user_balance"`
}
