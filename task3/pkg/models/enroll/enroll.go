package enroll

type Enroll struct {
	Id     int     `json:"id"`
	Amount float32 `json:"amount"`
}
