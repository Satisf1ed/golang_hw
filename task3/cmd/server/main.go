package main

import (
	"context"
	"os/signal"
	"syscall"
	"task3/internal/app/server"
	"time"
)

func main() {
	a := server.NewApp(":8080")

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	a.Run()

	<-ctx.Done()
	ctx, _ = context.WithTimeout(ctx, 3*time.Second)
	a.Stop(ctx)
}
