FROM golang:alpine
WORKDIR /build

COPY . .
RUN go build -o task3 ./cmd/server/main.go

CMD ["./task3"]