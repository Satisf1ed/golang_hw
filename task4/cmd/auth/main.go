package main

import (
	"flag"
	"github.com/anonimpopov/hw4/internal/app"
	logger2 "github.com/anonimpopov/hw4/internal/logger"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	"log"
	"net/http"
)

func getConfigPath() string {
	var configPath string

	flag.StringVar(&configPath, "c", "../../.config/auth.yaml", "path to config file")
	flag.Parse()

	return configPath
}

func main() {
	logger, err := logger2.GetLogger(false)
	if err != nil {
		log.Fatal(err)
	}

	config, err := app.NewConfig(getConfigPath())
	if err != nil {
		logger.Error("Unable to create config", zap.Error(err))
	}

	a, err := app.New(config)
	if err != nil {
		logger.Error("Unable to create app", zap.Error(err))
	}

	if err := a.Serve(); err != nil {
		logger.Error("Unable to run server", zap.Error(err))
	}

	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(":9000", nil)
}
