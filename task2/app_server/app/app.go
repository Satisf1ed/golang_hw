package app

import (
	"context"
	"fmt"
	"golang_hw/task2/app_server/http_server/handlers"
	"net/http"
)

type App struct {
	server *http.Server
}

func initServer() http.Handler {
	serverMux := http.NewServeMux()

	apiStruct := handlers.NewApi("0.1.0")
	hardOp := handlers.NewHardOp()
	decoder := handlers.NewDecoder()

	serverMux.Handle("/version", apiStruct)
	serverMux.Handle("/hard-op", hardOp)
	serverMux.Handle("/decode", decoder)

	return serverMux
}

func NewApp(address string) *App {
	newServer := &http.Server{
		Addr:    address,
		Handler: initServer(),
	}

	return &App{
		server: newServer,
	}
}

func (a *App) Run() {
	go func() {
		err := a.server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
}

func (a *App) Stop(ctx context.Context) {
	fmt.Println(a.server.Shutdown(ctx))
}
