package handlers

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type decoder struct {
	Input string `json:"inputString"`
}

type answer struct {
	Output string `json:"outputString"`
}

func (d *decoder) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		bytesBody, err := io.ReadAll(r.Body)

		if err != nil {
			w.Write([]byte("Плохое тело запроса"))
			fmt.Println(err)
		}

		var req decoder
		var respStruct answer
		err = json.Unmarshal(bytesBody, &req)
		if err != nil {
			fmt.Println(err)
		}

		ans, err := base64.StdEncoding.DecodeString(req.Input)
		respStruct.Output = string(ans)

		response, err := json.Marshal(respStruct)
		if err != nil {
			fmt.Println(err)
		}

		w.Write(response)
	}
}

func NewDecoder() *decoder {
	return &decoder{}
}
