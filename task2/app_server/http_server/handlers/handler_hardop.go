package handlers

import (
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

type hardOp struct {
	Time   time.Duration
	Status []int
}

func (h *hardOp) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.Time = time.Duration(rand.Intn(10) + 10)
	fmt.Println(h.Time)

	time.Sleep(time.Second * h.Time)
	index := rand.Int() % len(h.Status)
	w.Write([]byte(strconv.Itoa(h.Status[index])))
}

func NewHardOp() *hardOp {
	return &hardOp{
		Time:   1,
		Status: []int{http.StatusOK, http.StatusBadGateway, http.StatusInternalServerError},
	}
}
