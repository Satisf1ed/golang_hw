package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type apiVersion struct {
	Api string `json:"api_version"`
}

func (api *apiVersion) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	data, err := json.Marshal(api)
	if err != nil {
		fmt.Println(err)
	}

	w.Write(data)
}

func NewApi(api string) *apiVersion {
	return &apiVersion{
		Api: api,
	}
}
