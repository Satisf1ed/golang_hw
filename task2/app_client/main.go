package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

type request struct {
	Input string `json:"inputString"`
}

func decode() {
	var input string
	fmt.Print("Введите строку, которую хотите отправить: ")
	fmt.Scan(&input)
	encodedInput := base64.StdEncoding.EncodeToString([]byte(input))

	var req = &request{
		Input: encodedInput,
	}

	bytesRepresentation, err := json.Marshal(req)
	if err != nil {
		fmt.Println(err)
	}

	response, err := http.Post("http://127.0.0.1:8080/decode", "application/json", bytes.NewBuffer(bytesRepresentation))
	defer response.Body.Close()

	if err != nil {
		fmt.Println(err)
	}
	bytesResp, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Ответ сервера:", string(bytesResp))
}

func version() {
	response, err := http.Get("http://127.0.0.1:8080/version")
	if err != nil {
		fmt.Println(err)
	}
	defer response.Body.Close()

	data, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(data))
}

func hardOpChannel(ctx context.Context, done chan struct{}) {
	go func() { // Выполнение запроса
		response, err := http.Get("http://127.0.0.1:8080/hard-op")
		if err != nil {
			fmt.Println(err)
		}
		defer response.Body.Close()

		data, err := io.ReadAll(response.Body)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("true, " + string(data))

		close(done)
	}()

	select {
	case <-ctx.Done(): // Если лимит достигнут, прекращаем работу функции
		fmt.Println("false, Time limit exceeded.")
	case <-done: // Запрос успешен, сообщаем об этом пользователю
		fmt.Println("Request handled successfully.")
	}
}

func main() {
	version()
	decode()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	done := make(chan struct{})

	hardOpChannel(ctx, done)
}
